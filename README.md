# Serveur

## Télécharger
```
git clone (...)
sudo -s
pip3 install transformers
pip3 install transformers[torch]
pip3 install flask
pip3 install flask_cors
```

## Tester
```
$ python3 app.py
(le serveur démarre en mode dev ~30 secondes)
```

```
$ curl "http://127.0.0.1:5000/ask?question=ou%20est%20katia%20?"
{"answer":"dans ma mémoire vive avec les autres utilisateurs Scenari.","end":873,"score":0.4985193908214569,"start":815}
```

## Déployer en prod
https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uswgi-and-nginx-on-ubuntu-18-04

Config nginx :
```
        location /ask-backend/ {
                include uwsgi_params;
                # startup is very slow...
                uwsgi_read_timeout 300s;
                uwsgi_send_timeout 301s;
                uwsgi_pass unix:/opt/fquadweb/fquadweb.sock;
        }
        location /ask/ {
                rewrite  ^/ask/(.*) /$1 break;
                root /opt/fquadweb/frontend;
        }
```

Pour la config uwsgi, faire un lien symbolique avec uwsgi.ini du répertoire de l'application
Paramètre lazy-apps très important pour forker le process très tôt, car tensorflow ne fonctionne pas dans une instance python multithreadée.

# Client
Dans index.html, changer l'adresse qu'il va utiliser pour pointer sur le backend :
```
        $.ajax({
                url : 'https://scripts.scenari.org/ask-backend/ask',
```

