from transformers import AutoTokenizer, AutoModelForQuestionAnswering
import torch

from transformers import pipeline


from flask import Flask, jsonify, request, Response, make_response
from flask_cors import CORS



app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
app.config["APPLICATION_ROOT"] = "/ask-backend" # NO TRAILING SLASH !

CORS(app)

@app.errorhandler(404)
def page_not_found(e):
    return "L'app fonctionne, mais ce n'est pas la bonne addresse (404). Chemin: "+request.path, 404

nlp = pipeline('question-answering', 'illuin/camembert-large-fquad', 'illuin/camembert-large-fquad')

with open('context.txt', 'r') as file:
    context = file.read()

@app.route(app.config["APPLICATION_ROOT"]+'/')
def hello():
    resp=make_response('{"answer":"App chargée, utilisez /ask?question=(...)"}', 200)
    return resp


def askthink(question):

    answer=nlp({
        'question': question,
        'context': context
    })

    return answer

@app.route(app.config["APPLICATION_ROOT"]+'/ask', methods=['GET'])
def ask():
    headers='content_type: application/json; charset=utf-8'
    question = request.args['question']

    if question:
            ret=askthink(question)
            if (not ret):
                resp=make_response('{"answer":"Mes réponses sont limitées. Vous devez poser la bonne question."}', 200)
            else:
                resp=make_response(ret, 200)
    else:
        resp=make_response('{"answer":"J\'ai du mal a répondre, je pense que j\'ai pété un cable"}', 500)
    resp.headers.set=headers
    return resp


if __name__ == '__main__':
    app.run(host='0.0.0.0')

